#!/bin/bash
# Startskript fuer den Raspberry Pi
# Startet Java ohne Neukompilierung. Nutzen, wenn die Datenbank schon läuft und der Code kompiliert ist.

cd System/
if [ "$1" == "debug" ]; then
  export JAVA_OPTS="-agentlib:jdwp=transport=dt_socket,server=y,suspend=y,address=*:5005"
fi
sudo --preserve-env=JAVA_OPTS ./entrypoint.sh localhost # sudo-Rechte werden für den Zugriff auf GPIO benoetigt