USE ultimate_alarm_system;

TRUNCATE TABLE hw_components;

INSERT INTO hw_components(type, name, other)
VALUES ('Dummy', 'ArmingButton', '5'),
       ('Dummy', 'TriggerSensor', '10'),
       ('Dummy', 'ResetButton', '15');
