package de.davidjalbers.ultimatealarmsystem.api;

/**
 * Repräsentiert das Ergebnis einer Anfrage über die Web-API.
 * Eine solche kann Anfrage kann erfolgreich verlaufen sein oder auch nicht.
 */
public class WebServiceResult {

    private boolean successful;

    /**
     * Erzeugt ein neues Ergebnis mit dem Erfolgsstatus {@code false}.
     *
     * @see #isSuccessful()
     * @see #setSuccessful(boolean)
     */
    public WebServiceResult() {
        this(false);
    }

    /**
     * Erzeugt ein neues Ergebnis mit dem spezifizierten Erfolgsstatus.
     * @param successful {@code true}, falls die Anfrage erfolgreich war, sonst {@code false}
     *
     * @see #isSuccessful()
     * @see #setSuccessful(boolean)
     */
    public WebServiceResult(boolean successful) {
        this.successful = successful;
    }

    /**
     * Gibt den Erfolgsstatus der Anfrage zurück.
     * @return {@code true}, falls die Anfrage erfolgreich war, sonst {@code false}
     */
    public boolean isSuccessful() {
        return successful;
    }

    /**
     * Setzt den Erfolgsstatus der Anfrage.
     * @param successful {@code true}, falls die Anfrage erfolgreich war, sonst {@code false}
     */
    public void setSuccessful(boolean successful) {
        this.successful = successful;
    }
}
