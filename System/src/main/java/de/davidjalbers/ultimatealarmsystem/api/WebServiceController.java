package de.davidjalbers.ultimatealarmsystem.api;

import de.davidjalbers.ultimatealarmsystem.AlarmStateModel;
import de.davidjalbers.ultimatealarmsystem.data.HardwareComponent;
import de.davidjalbers.ultimatealarmsystem.data.HardwareComponentRepository;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.GenericEntity;
import jakarta.ws.rs.core.MediaType;

import java.util.List;
import java.util.logging.Logger;

/**
 * Der Controller für die Web-API.
 * Wird von der JAX-RS-Implementierung instanziiert
 */
@Path("/api")
public class WebServiceController {

    private static final Logger logger = Logger.getLogger(WebServiceController.class.getSimpleName());

    private static volatile boolean reloadRequested;

    private static volatile boolean stopRequested;

    /**
     * Gibt den aktuellen Zustand des Alarmsystems zurück.
     * @return den aktuellen Zustand des Alarmsystems
     */
    @GET
    @Path("/status")
    @Produces(MediaType.APPLICATION_JSON)
    public AlarmStateModel getState() {
        return AlarmStateModel.getInstance();
    }

    /**
     * Schaltet die Alarmanlage scharf bzw. nicht scharf.
     * @return das Ergebnis der Anfrage
     */
    @POST
    @Path("/status/arm")
    @Produces(MediaType.APPLICATION_JSON)
    public WebServiceResult arm() {
        if (AlarmStateModel.getInstance().isArmed()) {
            AlarmStateModel.getInstance().disarm();
        } else {
            AlarmStateModel.getInstance().arm();
        }
        return new WebServiceResult(true);
    }

    /**
     * Setzt einen eventuell ausgeloesten Alarm zurück.
     * @return das Ergebnis der Anfrage
     */
    @POST
    @Path("/status/reset")
    @Produces(MediaType.APPLICATION_JSON)
    public WebServiceResult reset() {
        AlarmStateModel.getInstance().reset();
        return new WebServiceResult(true);
    }

    /**
     * Löst einen Alarm aus, falls das Alarmsystem scharf geschaltet ist.
     * @return das Ergebnis der Anfrage (erfolgreich bei ausgeloestem Alarm)
     */
    @POST
    @Path("/status/trigger")
    @Produces(MediaType.APPLICATION_JSON)
    public WebServiceResult trigger() {
        AlarmStateModel.getInstance().attemptTrigger();
        return new WebServiceResult(AlarmStateModel.getInstance().isArmed());
    }

    /**
     * Gibt eine Liste aller in der Datenbank aufgeführten Komponenten aus.
     * @return eine Liste aller in der Datenbank aufgeführten Komponenten
     *
     * @see HardwareComponent
     */
    @GET
    @Path("/hardware")
    @Produces(MediaType.APPLICATION_JSON)
    public List<HardwareComponent> getComponents() {
        return HardwareComponentRepository.getHardwareComponents();
    }

    /**
     * Fuegt eine neue Hardwarekomponente zur Datenbank hinzu.
     * Diese wird beim nächsten Start oder Reload eingelesen und berücksichtigt.
     *
     * @param component die hinzuzufügende Komponente
     * @return das Ergebnis der Anfrage
     *
     * @see #stop()
     * @see #reload()
     */
    @PUT
    @Path("/hardware")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public WebServiceResult addComponent(HardwareComponent component) {
        return new WebServiceResult(HardwareComponentRepository.addHardwareComponent(component.getType(), component.getName(), component.getOther()));
    }

    /**
     * Gibt eine bestimmte Hardwarekomponente aus der Datenbank zurück.
     *
     * @param cID die ID der Komponente
     * @return die Hardwarekomponente. Loest eine RuntimeException aus, falls kein Komponente mit der
     * spezifizierten ID vorhanden ist.
     */
    @GET
    @Path("/hardware/{cID}")
    @Produces(MediaType.APPLICATION_JSON)
    public HardwareComponent getComponent(@PathParam("cID") int cID) {
        return HardwareComponentRepository.getHardwareComponent(cID);
    }


    /**
     * Löscht eine bestimmte Hardwarekomponente aus der Datenbank.
     *
     * @param cID die ID der Komponente
     * @return das Ergebnis der Anfrage
     *
     * @see #reload()
     */
    @DELETE
    @Path("/hardware/{cID}")
    @Produces(MediaType.APPLICATION_JSON)
    public WebServiceResult deleteComponent(@PathParam("cID") int cID) {
        return new WebServiceResult(HardwareComponentRepository.deleteHardwareComponent(cID));
    }


    /**
     * Aktualisiert eine bestimmte Hardwarekomponente in der Datenbank.
     *
     * @param cID die ID der Komponente
     * @return das Ergebnis der Anfrage
     *
     * @see #reload()
     */
    @POST
    @Path("/hardware/{cID}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public WebServiceResult updateComponent(@PathParam("cID") int cID, HardwareComponent component) {
        return new WebServiceResult(HardwareComponentRepository.updateHardwareComponent(cID, component.getType(), component.getName(), component.getOther()));
    }

    /**
     * Setzt ein Flag, das den Controller über einen gewünschten Reload informiert.
     * Bei einem Reload werden alle Views entfernt und neu aus der Datenbank aufgebaut.
     * Nach einer Änderung der Hardwarekonfiguration in der Datenbank kann statt einem echten Neustart ein
     * Reload ausgeführt werden, welcher den aktuellen Zustand des Alarmsystems (das {@link AlarmStateModel} erhält.
     * @return das Ergebnis der Anfrage
     */
    @POST
    @Path("/reload")
    @Produces(MediaType.APPLICATION_JSON)
    public WebServiceResult reload() {
        reloadRequested = true;
        return new WebServiceResult(true);
    }

    /**
     * Setzt ein Flag, das den Controller über einen gewünschten Stop informiert.
     * Im Gegensatz zum Kill erhält der Controller hier Gelegenheit, die Views vernünftig
     * herunterzufahren, zum Beispiel GPIO-Pins zurückzusetzen und Web Requests abzuschließen.
     * @return das Ergebnis der Anfrage
     *
     * @see #kill()
     */
    @POST
    @Path("/stop")
    @Produces(MediaType.APPLICATION_JSON)
    public WebServiceResult stop() {
        stopRequested = true;
        return new WebServiceResult(true);
    }

    /**
     * Diese Anfrage sorgt für ein sofortiges Beenden der JVM.
     *
     * <strong>Achtung! Hierdurch verweilen eventuell genutzte GPIO-Pins in ihrem aktuellen Zustand.
     * Der Controller räumt nicht auf und auch die Web-API wird sofort beendet. Nur nutzen, falls {@link #stop()}
     * nicht zum gewünschten Ergebnis führt.</strong>
     *
     * @see #stop()
     */
    @POST
    @Path("/kill")
    public void kill() {
        logger.severe("Following immediate runtime kill request... Goodbye");
        System.exit(0);
    }

    /**
     * Gibt den Zustand des Reload-Flags zurück.
     * Falls ein Reload angefordert war (d. h. {@code true} zurückgegeben wird,
     * wird der Zustand des Flags zurückgesetzt. Ein zweimaliges Aufrufen dieser Methode
     * führt also spätestens beim zweiten Mal zu {@code false} (falls zwischenzeitlich kein neuer Reload
     * beantragt wurde).
     * @return den Zustand des Reload-Flags
     *
     * @see #reload()
     */
    public static boolean isReloadRequested() {
        if (reloadRequested) {
            reloadRequested = false;
            return true;
        }
        return false;
    }

    /**
     * Gibt den Zustand des Stop-Flags zurück.
     * Falls ein Stop angefordert war (d. h. {@code true} zurückgegeben wird,
     * wird der Zustand des Flags zurückgesetzt. Ein zweimaliges Aufrufen dieser Methode
     * führt also spätestens beim zweiten Mal zu {@code false} (falls zwischenzeitlich kein neuer Stop
     * beantragt wurde). In der Regel ist dieses Verhalten jedoch irrelevant, weil das Programm nach einer
     * Stop-Anfrage ohnehin kontrolliert beendet werden sollte.
     * @return den Zustand des Stop-Flags
     *
     * @see #stop()
     */
    public static boolean isStopRequested() {
        if (stopRequested) {
            stopRequested = false;
            return true;
        }
        return false;
    }
}
