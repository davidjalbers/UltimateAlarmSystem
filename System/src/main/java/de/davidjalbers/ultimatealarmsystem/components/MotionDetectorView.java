package de.davidjalbers.ultimatealarmsystem.components;

import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.PinPullResistance;
import com.pi4j.io.gpio.PinState;
import de.davidjalbers.ultimatealarmsystem.GPIO;
import de.davidjalbers.ultimatealarmsystem.IView;

/**
 * Ein per GPIO an einen Raspberry Pi angeschlossener Bewegungssensor.
 * Wird der Sensor ausgeloest, wird ein zuvor gesetztes Runnable aufgerufen.
 */
public class MotionDetectorView implements IView {

    private final Runnable requestHandler;

    private final GpioPinDigitalInput input;

    /**
     * Erzeugt einen neuen Bewegungssensor, der auf einem bestimmten GPIO-Pin lauscht und Aenderungen am Zustand dieses Pins an
     * einen Ereignishandler weiterleitet.
     * @param requestHandler der ueber Bewegungen zu informierende Ereignishandler
     * @param pin der zu ueberwachende Pin
     *
     * @see com.pi4j.io.gpio.RaspiPin
     * @see com.pi4j.io.gpio.RaspiBcmPin
     */
    public MotionDetectorView(Runnable requestHandler, Pin pin) {
        this.input = GPIO.getController().provisionDigitalInputPin(pin, "MotionDetectorView", PinPullResistance.PULL_DOWN);
        this.input.setShutdownOptions(true, PinState.LOW);
        this.requestHandler = requestHandler;
    }

    private long lastHigh = 0;

    @Override
    public void poll() {
        if (input.isHigh()) {
            // Hardwarebedingt loest der Bewegungssensor nicht nur einmal aus, sondern 2-3 Sekunden lang konstant.
            // 5 Sekunden sollten locker ausreichen, um nicht mehrere Alarme hintereinander zu triggern.
            long currentHigh = System.currentTimeMillis();
            if (currentHigh - lastHigh > 5000) {
                lastHigh = currentHigh;
                requestHandler.run();
            }
        }
    }

    @Override
    public void clean() {
        GPIO.getController().unprovisionPin(input);
    }

    @Override
    public void update() {}
}
