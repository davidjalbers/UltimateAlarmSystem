package de.davidjalbers.ultimatealarmsystem.components;

import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.PinState;
import de.davidjalbers.ultimatealarmsystem.GPIO;
import de.davidjalbers.ultimatealarmsystem.IView;

/**
 * Ein über GPIO an den Raspberry Pi angeschlossener Summer.
 * Der Zustand des Summers wird über eine spezielle {@link DataSource} abgerufen, die zum Zeitpunkt des Konstruktors
 * bekannt sein muss.
 */
public class BuzzerView implements IView {

    private final DataSource dataSource;

    private final GpioPinDigitalOutput output;

    /**
     * Erzeugt einen neuen Summer mit der spezifizierten DataSource am spezifizierten Pin.
     * @param dataSource die Informationsquelle, aus der der Summer seinen Zustand beziehen soll
     * @param pin der Pin, an dem der Summer angeschlossen ist
     */
    public BuzzerView(DataSource dataSource, Pin pin) {
        this.output = GPIO.getController().provisionDigitalOutputPin(pin, "BuzzerView", PinState.LOW);
        this.output.setShutdownOptions(true, PinState.LOW);
        this.dataSource = dataSource;
    }

    @Override
    public void update() {
        if (dataSource.isActive()) {
            output.high();
        } else {
            output.low();
        }
    }

    @Override
    public void clean() {
        output.low(); // Der Summer trötet sonst weiter
        GPIO.getController().unprovisionPin(output);
    }

    /**
     * Informationsquelle für den Summer.
     * Hält den aktuellen Zustand des Summers.
     */
    public interface DataSource {

        /**
         * Gibt den aktuellen Zustand des Summers zurück.
         * @return {@code true}, falls der Summer aktiv sein soll, sonst {@code false}
         */
        boolean isActive();
    }

}
