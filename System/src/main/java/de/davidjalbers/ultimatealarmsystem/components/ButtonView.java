package de.davidjalbers.ultimatealarmsystem.components;

import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.PinState;
import de.davidjalbers.ultimatealarmsystem.GPIO;
import de.davidjalbers.ultimatealarmsystem.IView;

/**
 * Ein per GPIO an einen Raspberry Pi angeschlossener Taster.
 * Wird der Taster gedrueckt, wird ein zuvor gesetztes {@link Runnable} aufgerufen.
 */
public class ButtonView implements IView {

    private final Runnable requestHandler;

    private final GpioPinDigitalInput input;

    /**
     * Erzeugt einen neuen Taster, der auf einem bestimmten GPIO-Pin lauscht und Aenderungen am Zustand dieses Pins an
     * einen Ereignishandler weiterleitet.
     * @param requestHandler der ueber Knopfdruecke zu informierende Ereignishandler
     * @param pin der zu ueberwachende Pin
     *
     * @see com.pi4j.io.gpio.RaspiPin
     * @see com.pi4j.io.gpio.RaspiBcmPin
     */
    public ButtonView(Runnable requestHandler, Pin pin) {
        this.input = GPIO.getController().provisionDigitalInputPin(pin, "ButtonView");
        this.input.setShutdownOptions(true, PinState.LOW);
        this.requestHandler = requestHandler;
    }

    private boolean wasPreviouslyHigh;

    @Override
    public void clean() {
        GPIO.getController().unprovisionPin(input);
    }

    @Override
    public void poll() {
        if (input.isHigh()) {
            if (!wasPreviouslyHigh) {
                wasPreviouslyHigh = true;
                requestHandler.run();
            }
        } else {
            wasPreviouslyHigh = false;
        }
    }
}
