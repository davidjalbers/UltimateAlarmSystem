package de.davidjalbers.ultimatealarmsystem.inject;

import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.RaspiPin;
import de.davidjalbers.ultimatealarmsystem.IView;
import de.davidjalbers.ultimatealarmsystem.components.ButtonView;
import de.davidjalbers.ultimatealarmsystem.components.BuzzerView;
import de.davidjalbers.ultimatealarmsystem.components.DummyView;
import de.davidjalbers.ultimatealarmsystem.components.MotionDetectorView;
import de.davidjalbers.ultimatealarmsystem.data.HardwareComponent;
import de.davidjalbers.ultimatealarmsystem.data.HardwareComponentRepository;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.logging.Logger;

/**
 * Diese Utility-Klasse erzeugt aus den {@link HardwareComponent}s der {@link HardwareComponentRepository}
 * eine Liste von Views. Sie kümmert sich um die Umsetzung der {@link DataSourceProvider}- und {@link RequestHandler}-Annotationen.
 *
 * @see #setupViews(Object)
 */
public final class ComponentService {

    private static final Logger logger = Logger.getLogger(ComponentService.class.getSimpleName());

    // Instanziierung verhindern
    private ComponentService() {}

    /**
     * Erzeugt aus der Liste der HardwareComponents aus der {@link HardwareComponentRepository}.
     * Das übergebene Objekt wird nach DataSourceProvider- und RequestHandler-Methoden durchsucht,
     * die für die jeweiligen Namen der HardwareComponents ({@link DataSourceProvider#views()} und {@link RequestHandler#views()})
     * annotiert sind. Bei der Erzeugung der Views werden dann die RequestHandler gesetzt bzw. die DataSourceProvider
     * aufgerufen.
     *
     * <strong>Achtung! Die erzeugten Views werden noch nicht zum Controller hinzugefuegt!
     * Das übergebene Controller-Object dient nur der Suche nach DataSourceProvidern und RequestHandlern.</strong>
     *
     * @param controller das nach Methoden zu durchsuchende Controller-Objekt
     * @return die Liste der erzeugten Views
     */
    public static List<IView> setupViews(Object controller) {
        List<IView> views = new LinkedList<>();
        for (HardwareComponent component : HardwareComponentRepository.getHardwareComponents()) {
            views.add(createView(
                    component.getType(),
                    findListener(component.getName(), controller),
                    findDataSource(component.getName(), controller),
                    component.getOther()
            ));
        }
        return views;
    }

    private static Object findDataSource(String name, Object controller) {
        for (Method candidate : controller.getClass().getMethods()) {
            if (candidate.isAnnotationPresent(DataSourceProvider.class) && candidate.getReturnType() != Void.TYPE && candidate.getParameterCount() == 0) {
                for (String view : candidate.getDeclaredAnnotation(DataSourceProvider.class).views()) {
                    if (view.equals(name)) {
                        try {
                            Object dataSource = candidate.invoke(controller);
                            logger.fine("Invoked data source provider method " + candidate.getName() + " on " + controller);
                            return dataSource;
                        } catch (IllegalAccessException | InvocationTargetException e) {
                            throw new IllegalStateException("Failure on obtaining data source from method " + candidate.getName() + " on " + controller, e);
                        }
                    }
                }
            }
        }
        return null;
    }

    private static Runnable findListener(String name, Object controller) {
        for (Method candidate : controller.getClass().getMethods()) {
            if (candidate.isAnnotationPresent(RequestHandler.class) && candidate.getReturnType() == Void.TYPE && candidate.getParameterCount() == 0) {
                for (String view : candidate.getDeclaredAnnotation(RequestHandler.class).views()) {
                    if (view.equals(name)) {
                        return () -> {
                            try {
                                candidate.invoke(controller);
                                logger.fine("Invoked listener method " + candidate.getName() + "() on " + controller);
                            } catch (IllegalAccessException | InvocationTargetException e) {
                                throw new IllegalStateException("Failure on invoking listener method " + candidate.getName() + " on " + controller, e);
                            }
                        };
                    }
                }
            }
        }
        return null;
    }

    private static IView createView(String type, Runnable clickListener, Object dataSource, String other) {
        switch (type) {
            case "Button":
                return new ButtonView(Objects.requireNonNull(clickListener, "Request handler not found"), pinFromString(other));
            case "Dummy":
                return new DummyView(Objects.requireNonNull(clickListener, "Request handler not found"), Integer.parseInt(other));
            case "MotionDetector":
                return new MotionDetectorView(Objects.requireNonNull(clickListener, "Data source not found"), pinFromString(other));
            case "Buzzer":
                return new BuzzerView((BuzzerView.DataSource) Objects.requireNonNull(dataSource, "Data source not found"), pinFromString(other));
            default:
                throw new IllegalArgumentException("Invalid component type " + type);
        }
    }

    private static Pin pinFromString(String s) {
        try {
            return (Pin) RaspiPin.class.getField(s).get(null);
        } catch (Exception e) {
            return null;
        }
//        switch (s) {
//            case "GPIO_00": return RaspiPin.GPIO_00;
//            case "GPIO_01": return RaspiPin.GPIO_01;
//            case "GPIO_02": return RaspiPin.GPIO_02;
//            case "GPIO_03": return RaspiPin.GPIO_03;
//            case "GPIO_04": return RaspiPin.GPIO_04;
//            case "GPIO_05": return RaspiPin.GPIO_05;
//            case "GPIO_06": return RaspiPin.GPIO_06;
//            case "GPIO_07": return RaspiPin.GPIO_07;
//            case "GPIO_08": return RaspiPin.GPIO_08;
//            case "GPIO_09": return RaspiPin.GPIO_09;
//            case "GPIO_10": return RaspiPin.GPIO_10;
//            case "GPIO_11": return RaspiPin.GPIO_11;
//            case "GPIO_12": return RaspiPin.GPIO_12;
//            case "GPIO_13": return RaspiPin.GPIO_13;
//            case "GPIO_14": return RaspiPin.GPIO_14;
//            case "GPIO_15": return RaspiPin.GPIO_15;
//            case "GPIO_16": return RaspiPin.GPIO_16;
//            case "GPIO_17": return RaspiPin.GPIO_17;
//            case "GPIO_18": return RaspiPin.GPIO_18;
//            case "GPIO_19": return RaspiPin.GPIO_19;
//            case "GPIO_20": return RaspiPin.GPIO_20;
//            case "GPIO_21": return RaspiPin.GPIO_21;
//            case "GPIO_22": return RaspiPin.GPIO_22;
//            case "GPIO_23": return RaspiPin.GPIO_23;
//            case "GPIO_24": return RaspiPin.GPIO_24;
//            case "GPIO_25": return RaspiPin.GPIO_25;
//            case "GPIO_26": return RaspiPin.GPIO_26;
//            case "GPIO_27": return RaspiPin.GPIO_27;
//            case "GPIO_28": return RaspiPin.GPIO_28;
//            case "GPIO_29": return RaspiPin.GPIO_29;
//            case "GPIO_30": return RaspiPin.GPIO_30;
//            case "GPIO_31": return RaspiPin.GPIO_31;
//            default:
//                return null;
//        }
    }

}
