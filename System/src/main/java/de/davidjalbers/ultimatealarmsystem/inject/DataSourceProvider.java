package de.davidjalbers.ultimatealarmsystem.inject;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Definiert die mit dieser Annotation annotierte Methode als DataSourceProvider für bestimmte Views (siehe {@link #views()}).
 * Die Methode darf keine Parameter entgegennehmen und der Rückgabetyp muss zur entsprechenden DataSource gecastet werden können
 * (ClassCastException droht).
 *
 * Achtung! Alle spezifizierten Views muessen vom selben Typ sein (sonst schlaegt der Cast zur jeweiligen DataSource fehl)!
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface DataSourceProvider {

    /**
     * Gibt die Namen der Views zurück, für die diese Methode eine DataSource bereitstellt.
     * Alle Views müssen später vom selben Typ sein.
     * @return die Namen der Views dieses DataSourceProviders
     */
    String[] views();
}
