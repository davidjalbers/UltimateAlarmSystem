package de.davidjalbers.ultimatealarmsystem;

import com.sun.net.httpserver.HttpServer;
import de.davidjalbers.ultimatealarmsystem.api.WebServiceController;
import de.davidjalbers.ultimatealarmsystem.data.HardwareComponentRepository;
import de.davidjalbers.ultimatealarmsystem.inject.ComponentService;
import org.glassfish.jersey.jdkhttp.JdkHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

import java.net.URI;
import java.util.logging.Logger;

/**
 * Dummy-Klasse. Enthaelt nur den Einstiegspunkt.
 */
public final class Startup {

    private static final Logger logger = Logger.getLogger(Startup.class.getSimpleName());

    // Instanziierung verhindern
    private Startup() {}

    /**
     * Einstiegspunkt ins Programm.
     * Startet Datenbankverbindung und die Web-API.
     * Erzeugt dann den Controller und überwacht seinen Lebenszyklus.
     *
     * @param args Konsolenargumente (nicht verwendet)
     */
    public static void main(String[] args) {

        // Verbindung zur Datenbank aufbauen
        HardwareComponentRepository.setupConnection("hw_components",
                // Fuer Docker
                "jdbc:mysql://uas-database:3306/ultimate_alarm_system?user=uas_user&password=uas_2020",
                // Fuer Nicht-Docker
                "jdbc:mysql://localhost:3306/ultimate_alarm_system?user=uas_user&password=uas_2020"
        );

        // Web-API starten
        HttpServer webServer = JdkHttpServerFactory.createHttpServer(
                URI.create("http://localhost:8080/"),
                new ResourceConfig().packages("de.davidjalbers.ultimatealarmsystem.api")
        );

        // Alarmsytem starten und auf Stops und Reloads hoeren
        Thread controllerThread = null;
        AlarmSystemController controller = new AlarmSystemController();
        while (!WebServiceController.isStopRequested()) {
           if (WebServiceController.isReloadRequested() || controllerThread == null) {
               if (controllerThread != null) {
                   controllerThread.interrupt();
                   try {
                       controllerThread.join();
                   } catch (InterruptedException e) {
                       throw new RuntimeException("Something went seriously wrong. Main thread interrupted while joining on controller thread", e);
                   }
                   logger.info("Reloading...");
               }
               controller.addViews(ComponentService.setupViews(controller));
               controllerThread = controller.launch();
           }
        }
        logger.warning("Stopping alarm system runtime...");

        // An dieser Stelle soll das System gestoppt werden.

        // Alarmsystem stoppen
        if (controllerThread != null) {
            controllerThread.interrupt();
        }
        // Webserver stoppen, mit einer Sekunde Gnadenfrist
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) { e.printStackTrace(); }
        webServer.stop(0); // Zusammenhang mit https://bugs.openjdk.java.net/browse/JDK-8233185?
        // JVM töten (unter Docker scheint das bei diesem Image nicht zu passieren)
        System.exit(0);
    }

}
