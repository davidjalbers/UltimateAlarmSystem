package de.davidjalbers.ultimatealarmsystem.data;

import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;

/**
 * Repräsentiert den Datenbankeintrag für eine am Raspberry Pi angeschlossene Hardwarekomponente.
 *
 * Diese Klasse ist nicht zu verwechseln mit den konkreten Views im Paket {@code de.davidjalbers.ultimatealarmsystem.components}.
 * Sie enthält, wie die Datenbank, nur Strings.
 * Zur Umwandlung einer {@link HardwareComponent} in die entsprechende {@link de.davidjalbers.ultimatealarmsystem.IView} müssen auch Listener und DataSources
 * im Controller gelesen und gesetzt werden. Hierum kümmern sich die Klassen im Paket
 * {@code de.davidjalbers.ultimatealarmsystem.inject}.
 */
@XmlRootElement
public class HardwareComponent {

    private int cID;

    private String type;

    private String name;

    private String other;

    /**
     * Erzeugt eine neue Hardwarekomponente mit den spezifizierten Parametern.
     * Für den Typ werden aktuell folgende Werte unterstützt:
     *
     * <ul>
     *  <li>{@code "Button"}</li>
     *  <li>{@code "Buzzer"}</li>
     *  <li>{@code "MotionDetector"}</li>
     *  <li>und {@code "Dummy"}.</li>
     * </ul>
     *
     * @param cID die ID der Komponente (Primärschlüssel in der Datenbank)
     * @param type der Typ der Komponente
     * @param name der Name der Komponente. Anhand dessen wird im Controller nach entsprechend annotierten
     *             {@link de.davidjalbers.ultimatealarmsystem.inject.DataSourceProvider}n und
     *             {@link de.davidjalbers.ultimatealarmsystem.inject.RequestHandler}n gesucht.
     * @param other weitere Informationen über die Komponente. Für simple Hardware ist das zum Beispiel der
     *              verwendete Port im Format {@code "GPIO_XX"}, für Dummys die Anzahl in Sekunden, nach der sie
     *              auslösen.
     */
    public HardwareComponent(int cID, String type, String name, String other) {
        this.cID = cID;
        this.type = type;
        this.name = name;
        this.other = other;
    }

    public HardwareComponent() {}

    /**
     * Gibt die ID dieser Komponente (Primärschlüssel der Datenbank) zurück.
     * @return die ID dieser Komponente
     */
    public int getcID() {
        return cID;
    }

    public void setcID(int cID) {
        this.cID = cID;
    }

    /**
     * Gibt den Typ dieser Komponente zurück. Aktuell werden folgende Werte unterstützt:
     * <ul>
     *  <li>{@code "Button"}</li>
     *  <li>{@code "Buzzer"}</li>
     *  <li>{@code "MotionDetector"}</li>
     *  <li>und {@code "Dummy"}.</li>
     * </ul>
     *
     * @return den Typ dieser Komponente
     */
    public String getType() {
        return type;
    }

    /**
     * Setzt den Typ dieser Komponente. Es wird beim Erzeugen der echten Views eine Exception geworfen, wenn der Typ
     * nicht einer der folgenden Werte ist:
     * <ul>
     *  <li>{@code "Button"}</li>
     *  <li>{@code "Buzzer"}</li>
     *  <li>{@code "MotionDetector"}</li>
     *  <li>oder {@code "Dummy"}.</li>
     * </ul>
     *
     * @param type der Typ dieser Komponente
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Gibt den Namen dieser Komponente zurück.
     * Anhand des Namens wird beim Erzeugen der echten Views der Controller nach mit
     * {@link de.davidjalbers.ultimatealarmsystem.inject.DataSourceProvider} oder {@link de.davidjalbers.ultimatealarmsystem.inject.RequestHandler}
     * annotierten Methoden gesucht.
     *
     * @return den Namen dieser Komponente
     */
    public String getName() {
        return name;
    }

    /**
     * Setzt den Namen dieser Komponente.
     * Anhand des Namens wird beim Erzeugen der echten Views der Controller nach mit
     * {@link de.davidjalbers.ultimatealarmsystem.inject.DataSourceProvider} oder {@link de.davidjalbers.ultimatealarmsystem.inject.RequestHandler}
     * annotierten Methoden gesucht.
     *
     * @param name der Name dieser Komponente
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gibt weitere Informationen über diese Komponente zurück (bspw. die Portnummer oder die Sekundenzahl bis zum Auslösen).
     * @return weitere Informationen über diese Komponente
     */
    public String getOther() {
        return other;
    }

    /**
     * Setzt die weiteren Informationen über diese Komponente (bspw. die Portnummer oder die Sekundenzahl bis zum Auslösen).
     * @param other die weiteren Informationen über diese Komponente
     */
    public void setOther(String other) {
        this.other = other;
    }
}
