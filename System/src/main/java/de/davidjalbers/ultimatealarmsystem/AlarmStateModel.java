package de.davidjalbers.ultimatealarmsystem;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Stellt den gegenwaertigen Zustand des Alarmsystems bereit.
 *
 * Das Alarmsystem hat die zwei Zustaende "scharf" und "nicht scharf" bzw. "armed" und "not armed".
 * Nur ein scharfes Alarmsystem kann ausgeloest werden ("triggered").
 *
 * @see #isArmed()
 * @see #isTriggered()
 */
public class AlarmStateModel {

    private static final Logger logger = Logger.getLogger(AlarmStateModel.class.getSimpleName());

    // Auskommentieren, um auch FINE Logs in die Konsole zu schreiben
    static {
        //logger.setLevel(Level.FINE);
        //Logger.getLogger("").getHandlers()[0].setLevel(Level.FINE);
    }

    private static AlarmStateModel instance;

    private boolean armed;

    private boolean triggered;

    public AlarmStateModel() {
        if (instance != null)
            throw new IllegalStateException("Another AlarmStateModel already exists");
        instance = this;
    }

    /**
     * Gibt den aktuellen Zustand des Alarmsystems zurueck.
     * @return {@code true}, falls das Alarmsystem scharfgestellt ist, sonst {@code false}
     */
    public boolean isArmed() {
        return armed;
    }

    /**
     * Zeigt an, ob das Alarmsystem ausgeloest wurde oder nicht.
     * Falls diese Methode {@code true} zurueckgibt, muss auch {@link #isArmed()} {@code true} zurueckgeben.
     * @return {@code true}, wenn das scharfe Alarmsystem ausgeloest wurde, sonst {@code false}
     */
    public boolean isTriggered() {
        return triggered;
    }

    /**
     * Setzt den Zustand des Alarmsystems auf "nicht scharf" und "nicht ausgeloest".
     * Diese Methode hebt sowohl die Scharfstellung als auch einen eventuell ausgeloesten Alarm auf.
     */
    public void disarm() {
        reset();
        if (armed)
            logger.info("System disarmed");
        armed = false;
    }
    
    /**
     * Setzt einen ausgeloesten Alarm zurueck (Zustand "nicht ausgeloest").
     * Das Alarmsystem bleibt "scharf".
     */
    public void reset() {
        if (triggered)
            logger.info("System reset");
        triggered = false;
    }

    /**
     * Stellt das Alarmsystem scharf, sodass es ausgeloest werden kann.
     * 
     * @see #attemptTrigger()
     */
    public void arm() {
        if (!armed)
            logger.info("System armed");
        armed = true;
    }

    /**
     * Loest den Alarm aus, falls das Alarmsystem scharfgestellt ist.
     * Ansonsten passiert nichts.
     */
    public void attemptTrigger() {
        if (armed) {
            if (!triggered)
                logger.severe("System alarmed");
            triggered = true;
        } else {
            logger.fine("Trigger registered but system is not armed");
        }
    }

    public static AlarmStateModel getInstance() {
        return instance;
    }
}
