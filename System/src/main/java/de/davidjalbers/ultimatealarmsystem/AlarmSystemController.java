package de.davidjalbers.ultimatealarmsystem;

import de.davidjalbers.ultimatealarmsystem.components.BuzzerView;
import de.davidjalbers.ultimatealarmsystem.inject.DataSourceProvider;
import de.davidjalbers.ultimatealarmsystem.inject.RequestHandler;

import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

/**
 * Der Controller des MVC-Alarmsystems.
 * Führt Models und Views zusammen.
 */
public class AlarmSystemController implements Runnable {

    private static final Logger logger = Logger.getLogger(AlarmSystemController.class.getSimpleName());

    private final AlarmStateModel state = new AlarmStateModel();

    private final List<IView> views = new LinkedList<>();

    public AlarmSystemController() {
        logger.info("Starting up...");
    }

    /**
     * Registriert Views beim Controller.
     * @param viewsToAdd hinzuzufuegende Views
     */
    public void addViews(List<IView> viewsToAdd) {
        views.addAll(viewsToAdd);
    }

    /**
     * Startet die {@link #run()}-Methode dieses Controllers auf einem neuen Thread und gibt diesen zurück.
     * Der Thread befindet sich von nun an in einer Endlosschleife, in der Views auf Updates geprüft und aktualisiert
     * werden. Durch das Setzen des Interrupts-Flags mit {@link Thread#interrupt()} kann die Schleife kontrolliert
     * verlassen werden. Durch erneutes Aufrufen dieser Methode wird dann ein neuer Thread erzeugt und der Controller
     * erneut gestartet (bspw. nach einem Reload).
     *
     * @return den Thread, auf dem dieser Controller läuft
     */
    public Thread launch() {
        Thread thread = new Thread(this);
        thread.start();
        return thread;
    }

    /**
     * Startet den Lebenszyklus des Controllers.
     *
     * Der Lebenszyklus ist eine Endlosschleife aus Updates der Views, und der Reaktion auf diese Updates.
     * Die Schleife kann durch einen Thread-Interrupt (siehe {@link #launch()}) kontrolliert verlassen werden.
     * Bei diesem kontrollierten Verlassen werden die GPIO-Pins der Hardware freigegeben und anschließend die Views
     * aus dem Controller geloescht (neue Views können mit {@link #addViews(List)} hinzugefügt werden).
     *
     * <strong>Achtung! Höchstwahrscheinlich sollte diese Methode nicht direkt, sondern über {@link #launch()} aufgerufen werden.</strong>
     *
     * @see #launch()
     * @see #addViews(List)
     */
    @Override
    public void run() {
        views.forEach(IView::update); // Views initialisieren
        logger.info("System is now running");
        while (!Thread.interrupted()) {
            views.forEach(IView::poll);
            views.forEach(IView::update);
        }
        logger.info("Cleaning up views");
        views.forEach(IView::clean);
        views.clear();
    }

    /**
     * Wird ausgeführt, wenn ein Bewegungsmelder ausgelöst wird und potenziell einen Alarm auslösen könnte.
     */
    @RequestHandler(views = "TriggerSensor")
    public void onTrigger() {
        state.attemptTrigger();
    }

    /**
     * Wird ausgeführt, wenn der Knopf für das Scharfstellen gedrückt wurde.
     * Schaltet zwischen "scharfgestellt" und "nicht scharfgestellt" um.
     */
    @RequestHandler(views = "ArmingButton")
    public void onArm() {
        if (!state.isArmed())
            state.arm();
        else if (true /*TODO add authorisation with RFID reader*/)
            state.disarm();
    }

    /**
     * Wird ausgeführt, wenn der Knopf zum Zurücksetzen eines ausgelösten Alarms gedrückt wird.
     */
    @RequestHandler(views = "ResetButton")
    public void onReset() {
        if (true /*TODO add authorisation with RFID reader*/)
            state.reset();
    }

    /**
     * Stellt die DataSource für den Summer bereit, der auf einen ausgelösten Alarm hindeutet.
     * @return die DataSource für den Alarm-Summer
     */
    @DataSourceProvider(views = "AlarmBuzzer")
    public BuzzerView.DataSource getAlarmDataSource() {
        return new BuzzerView.DataSource() {
            @Override
            public boolean isActive() {
                return state.isTriggered();
            }
        };
    }
}
