package de.davidjalbers.ultimatealarmsystem;

/**
 * Eine View nach MVC-Konzept.
 *
 * Eine View ist verantwortlich fuer das Darstellen von Daten aus dem Model und die Interaktion mit dem Benutzer.
 */
public interface IView {

    /**
     * Ueberprueft auf eingehende Anfragen des Benutzers.
     * Eine solche Anfrage ist zum Beispiel ein Knopfdruck oder ein Mausklick.
     *
     * Obwohl in dieser Methode prinzipiell alles geschehen kann, wird nach MVC-Konzept der Controller ueber die
     * Anfrage informiert. Eine implementierende View-Klasse sollte diese Methode ueberschreiben, falls sie Anfragen
     * vom Benutzer empfangen kann.
     */
    default void poll() {}

    /**
     * Ueberprueft auf Aenderungen in den Models und aktualisiert gegebenenfalls die Praesentation der Daten.
     *
     * Nach MVC-Konzept wird hier auf veraenderte Daten in den Models getestet und diese Aenderungen dem Benutzer
     * angezeigt, um Daten und Nutzerinterface synchronisiert zu halten. Eine implementierende View-Klasse sollte
     * diese Methode ueberschreiben, falls sie die Daten eines Models ueberwachen soll.
     */
    default void update() {}

    /**
     * Räumt die View auf.
     *
     * Diese Methode sollte eventuell von der View belegte Ressourcen, wie einen GPIO-Pin o. Ä., wieder freigeben.
     */
    default void clean() {}

}
