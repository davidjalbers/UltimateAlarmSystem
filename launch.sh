#!/bin/bash
# Launchskript für den Raspberry Pi
# Ausfuehren, wenn Java-Code oder Datenbankschema geaendert wurden
# Kompiliert das Java-Modul, aktualisiert (fall noetig) das Datenbank-Image und startet dann Datenbank und Java.
# Der Datenbank-Container und sein Volume werden nicht entfernt.

if [ "$1" != "nocompile" ]; then
    echo "SCHRITT 1 von 3: Java kompilieren"
    ./gradlew installDist
fi
echo "SCHRITT 2 von 3: Datenbank starten"
docker-compose up -d --build uas-database uas-phpmyadmin uas-web
echo "SCHRITT 3 von 3: Java starten"
./start.sh "$1"